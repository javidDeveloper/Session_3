package com.food.javid.foodlaziz;

import android.content.Context;
import android.widget.Toast;

public class ToastClass {


    public void EmptyAlarm(Context context) {
        Toast.makeText(context, R.string.dont_empty, Toast.LENGTH_SHORT).show();
    }

    public void ClearAlarm(Context context) {
        Toast.makeText(context, R.string.ok_shod, Toast.LENGTH_SHORT).show();
    }

    public void ErrorAlarm(Context context) {
        Toast.makeText(context, R.string.ErrorAlarm, Toast.LENGTH_SHORT).show();
    }

    public void startAlarm(Context context) {
        Toast.makeText(context, R.string.startAlarm, Toast.LENGTH_SHORT).show();
    }

    public void pauseAlarm(Context context) {
        Toast.makeText(context, R.string.pauseAlarm, Toast.LENGTH_SHORT).show();
    }

    public void stopAlarm(Context context) {
        Toast.makeText(context, R.string.stopAlarm, Toast.LENGTH_SHORT).show();
    }

    public void destoryAlarm(Context context) {
        Toast.makeText(context, R.string.destoryAlarm, Toast.LENGTH_SHORT).show();
    }

    public void reciveAlarm(Context context) {
        Toast.makeText(context, R.string.ReciveAlarm, Toast.LENGTH_SHORT).show();
    }

}

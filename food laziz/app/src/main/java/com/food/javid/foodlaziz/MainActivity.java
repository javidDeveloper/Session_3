package com.food.javid.foodlaziz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.food.javid.foodlaziz.ListFood.GridViewFoodActivity;
import com.food.javid.foodlaziz.ListFood.ListFoodActivity;
import com.food.javid.foodlaziz.Register.RegisterActivity;

public class MainActivity extends AppCompatActivity {
    Context mContext = MainActivity.this;
    TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        name.setText(getShared("name", "gust"));
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, RegisterActivity.class);
                startActivityForResult(intent, 100);
            }
        });

        findViewById(R.id.list_food).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ListFoodActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.grid_food).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GridViewFoodActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                String nameVal = data.getStringExtra("name");
                name.setText(nameVal);

            } else {
                Toast.makeText(mContext, "Data fail !!", Toast.LENGTH_SHORT).show();
            }
        }
    }
    String getShared(String name, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString(name, defaultValue);
    }
}

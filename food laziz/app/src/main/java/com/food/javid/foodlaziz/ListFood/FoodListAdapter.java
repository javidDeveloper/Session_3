package com.food.javid.foodlaziz.ListFood;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.javid.foodlaziz.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodListAdapter extends BaseAdapter {
    Context mContext;
    List<FoodModel> foods;

    public FoodListAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }


    public void addFood(FoodModel model){
        foods.add(model);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int i) {
        return foods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.foods_list_item, null);
        TextView name = v.findViewById(R.id.name);
        TextView price = v.findViewById(R.id.price);
        TextView more = v.findViewById(R.id.more);
        ImageView img = v.findViewById(R.id.img);

        name.setText(foods.get(i).getName());
        price.setText(foods.get(i).getPrice() + "");
        more.setText(foods.get(i).getMore());
        Picasso.with(mContext).load(foods.get(i).getImgAddress()).into(img);
        return v;
    }
}

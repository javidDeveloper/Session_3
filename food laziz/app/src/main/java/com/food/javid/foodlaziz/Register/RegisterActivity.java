package com.food.javid.foodlaziz.Register;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.food.javid.foodlaziz.R;

public class RegisterActivity extends AppCompatActivity {
    EditText name, cod;
    Context mContext = RegisterActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        name = findViewById(R.id.name);
        String nameValue = name.getText().toString();
        saveShared("name", nameValue);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameVal = name.getText().toString();
                Log.e("LOG", nameVal + "FromDestination");
                Intent saveIntent = new Intent();

                if (nameVal.length() == 0) {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                } else {
                    saveIntent.putExtra("name", nameVal);
                    setResult(Activity.RESULT_OK, saveIntent);
                    finish();
                    Toast.makeText(mContext, "Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    void saveShared(String key, String name) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                .putString(key, name).apply();
    }


}

package com.food.javid.foodlaziz.ListFood;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;

import com.food.javid.foodlaziz.R;
import com.food.javid.foodlaziz.ToastClass;

import java.util.ArrayList;
import java.util.List;

public class GridViewFoodActivity extends AppCompatActivity {
    String pizzaAddress = "https://littlecaesars.com/Portals/_default/Skins/Porto/Resources/img/home/pizzas-bg.jpg";
    String hamburgerAddress = "https://whatscookingamerica.net/wp-content/uploads/2015/05/Hamburger2.jpg";
    String hotdogAddress = "http://www.todayifoundout.com/wp-content/uploads/2014/01/hot-dog.jpg";
    GridView listFood;
    Context mContext = GridViewFoodActivity.this;
    EditText nameFood, priceFood, moreFood;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_food);
        //**//**//**//**// objects //**//**//**//**//**//
        nameFood = findViewById(R.id.name_food);
        priceFood = findViewById(R.id.price_food);
        moreFood = findViewById(R.id.more_food);
        listFood = findViewById(R.id.grid_food);


        final List<FoodModel> food = new ArrayList<>();

        FoodModel pizza = new FoodModel();
        pizza.setName("Pizza");
        pizza.setPrice(120000);
        pizza.setImgAddress(pizzaAddress);
        pizza.setMore("the best of name");
        food.add(pizza);

        FoodModel sandwich = new FoodModel();
        sandwich.setName("Sandwich Hamburger");
        sandwich.setPrice(100000);
        sandwich.setImgAddress(hamburgerAddress);
        sandwich.setMore("the best of name");
        food.add(sandwich);

        FoodModel hotdog = new FoodModel();
        hotdog.setName("HOTDog");
        hotdog.setPrice(90000);
        hotdog.setImgAddress(hotdogAddress);
        hotdog.setMore("the best of name");
        food.add(hotdog);
        final FoodListAdapter adapter = new FoodListAdapter(mContext, food);
        listFood.setAdapter(adapter);

        //**//**//**//**// add new row to current list //**//**//**//**//
        findViewById(R.id.add_food).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameVal = nameFood.getText().toString();
                String priceVal = priceFood.getText().toString();
                String moreVal = moreFood.getText().toString();

                FoodModel addFood = new FoodModel();

                if (nameFood.length() == 0 || priceFood.length() == 0 || moreFood.length() == 0) {
                    ToastClass toastClass = new ToastClass();
                    toastClass.EmptyAlarm(mContext);
                } else {
                    addFood.setName(nameVal);
                    addFood.setPrice(Integer.parseInt(priceVal));
                    addFood.setMore(moreVal);
                    adapter.addFood(addFood);
                    nameFood.setText("");
                    priceFood.setText("");
                    moreFood.setText("");
                    ToastClass toastClass = new ToastClass();
                    toastClass.reciveAlarm(mContext);
                }
            }
        });
    }
}
